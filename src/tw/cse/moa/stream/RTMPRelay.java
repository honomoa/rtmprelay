package tw.cse.moa.stream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RTMPRelay implements Runnable {

	private int rtmpPort = 1935;

	private int listenPort = 1935;

	private String server = "live.justin.tv";
	private InputStream xsplitInput, jtvInput;
	private OutputStream xsplitOutput, jtvOutput;

	private LocalServer ls;
	private StreamClient sc;

	private boolean isXsplitConnected = false;

	public RTMPRelay() throws IOException {
	}

	public RTMPRelay(int rtmpPort, int listenPort, String server) throws IOException {
		this.rtmpPort = rtmpPort;
		this.listenPort = listenPort;
		this.server = server;
	}

	public static void main(String[] args) {
		try {
			new RTMPRelay().run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			ls = new LocalServer(this);
			ls.start();
			sc = new StreamClient(this);
			sc.start();
			while (ls.isConnected() && !isXsplitConnected) {
				if (xsplitInput != null && jtvOutput != null) {
					new LocalProxy(xsplitInput, jtvOutput);
					new LocalProxy(jtvInput, xsplitOutput);
					isXsplitConnected = true;
				}
			}
			Thread.sleep(10);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void closeSocket() {
		try {
			sc.close();
			ls.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			sc = null;
			ls = null;
		}
	}

	public boolean isXsplitConnected() {
		return isXsplitConnected;
	}

	public int getRtmpPort() {
		return rtmpPort;
	}

	public void setRtmpPort(int rtmpPort) {
		this.rtmpPort = rtmpPort;
	}

	public int getListenPort() {
		return listenPort;
	}

	public void setListenPort(int listenPort) {
		this.listenPort = listenPort;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public InputStream getXsplitInput() {
		return xsplitInput;
	}

	public void setXsplitInput(InputStream inputStream) {
		this.xsplitInput = (inputStream);
	}

	public InputStream getJtvInput() {
		return jtvInput;
	}

	public void setJtvInput(InputStream inputStream) {
		this.jtvInput = (inputStream);
	}

	public OutputStream getXsplitOutput() {
		return xsplitOutput;
	}

	public void setXsplitOutput(OutputStream outputStream) {
		this.xsplitOutput = outputStream;
	}

	public OutputStream getJtvOutput() {
		return jtvOutput;
	}

	public void setJtvOutput(OutputStream outputStream) {
		this.jtvOutput = outputStream;
	}

}
