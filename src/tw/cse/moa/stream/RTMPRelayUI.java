package tw.cse.moa.stream;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class RTMPRelayUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7497287463381988708L;
	private JTextField listenTextField, rtmpTextField;
	@SuppressWarnings("unused")
	private JLabel serverListLabel, startListenLabel, connectedLabel;
	private JComboBox serverListCombo;
	private JButton startListenBtn;
	private RTMPRelayModel model;
	private RTMPRelayController controller;

//	private JTextArea composeTextArea;

	public RTMPRelayUI() {
		super("Stream Proxy");
		model = new RTMPRelayModel();
		controller = new RTMPRelayController();
		setUpUIComponent();
		setUpEventListener();
		setLocationRelativeTo(this);
		setVisible(true);
	}

	private void setUpUIComponent() {
		setSize(200, 170);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new FlowLayout(FlowLayout.LEFT));
		

		serverListCombo = new JComboBox(model.getServerListDisplay());
//		serverListCombo.setBackground(Color.gray);
//		serverListCombo.setForeground(Color.red);
		add(serverListLabel = new JLabel("Server List: "));
		add(serverListCombo);
		add(serverListLabel = new JLabel("Listen Port: "));
		add(listenTextField = new JTextField(8));
		listenTextField.setText("1935");
		add(serverListLabel = new JLabel("RTMP Port: "));
		add(rtmpTextField = new JTextField(8));
		rtmpTextField.setText("1935");
		add(connectedLabel = new JLabel("　　　　　　　　"));
		add(startListenBtn = new JButton("Start"));

//		composeTextArea = new JTextArea(15, 20);
//		composeTextArea.setLineWrap(true);
//		add(new JScrollPane(composeTextArea));
	}

	private void setUpEventListener() {
		String[] server = model.getServerList();
		controller.setServerListCombo(serverListCombo, server);
		controller.setListenTextField(listenTextField);
		controller.setRtmpTextField(rtmpTextField);
		controller.setStartListenBtn(startListenBtn);
	}

	public static void main(String[] args) throws IOException {
		new RTMPRelayUI();
	}
}
