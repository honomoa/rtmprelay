package tw.cse.moa.stream;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class LocalServer extends Thread {
	
	private RTMPRelay relay;
	private ServerSocket serverSocket;
	private Socket clientSocket; 
	private boolean isLocalServerStart;
	private boolean isConnected;
	
	public LocalServer (RTMPRelay relay) throws IOException {
		this.relay = relay;
		this.serverSocket = new ServerSocket(relay.getListenPort());
	}

	public void waitForXsplit() {
		while (true && isLocalServerStart) {// 永遠執行
			try {
				synchronized (serverSocket) {
					clientSocket = serverSocket.accept();
					if (clientSocket.isConnected()) {
						relay.setXsplitInput(clientSocket.getInputStream());
						relay.setXsplitOutput(clientSocket.getOutputStream());
						setConnected(true);
						System.out.println(clientSocket.getRemoteSocketAddress().toString() + " is connected!");
					}
					if (clientSocket.isClosed()) {
						setConnected(false);
						System.out.println(clientSocket.getRemoteSocketAddress().toString() + " is disconnected!");
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void run() {
		isLocalServerStart = true;
		waitForXsplit();
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public boolean isConnected() {
		return isConnected;
	}
	
	public void close() throws IOException {
		isLocalServerStart = false;
		if(clientSocket!=null) clientSocket.close();
		if(serverSocket!=null) serverSocket.close();
		clientSocket = null;
		serverSocket = null;
	}
}
