package tw.cse.moa.stream;

public class RTMPRelayModel {

	private String serverListDisplay[] = {"US West: San Francisco, CA","Global Load Balancing","Asia: Singapore"};
	private String serverList[] = {"live.justin.tv","live-3c.justin.tv","live-sin-backup.justin.tv"};

	public String[] getServerListDisplay() {
		return serverListDisplay;
	}
	public String[] getServerList() {
		return serverList;
	}
}
