package tw.cse.moa.stream;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class RTMPRelayController {
	private RTMPRelay relay;
	
	public RTMPRelayController() {
		try {
			relay = new RTMPRelay();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setStartListenBtn (final JButton startListenBtn) {
		startListenBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						try {
							if("Start".equals(startListenBtn.getText())) {
								startListenBtn.setText("Stop");
								relay.run();
							} else {
								startListenBtn.setText("Start");
								relay.closeSocket();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
	}

	public void setServerListCombo (final JComboBox serverListCombo, final String[] serverList) {
		serverListCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						try {
							setServer(serverList[serverListCombo.getSelectedIndex()]);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
	}

	public void setListenTextField(final JTextField listenTextField) {
		listenTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						try {
							setListenPort(listenTextField.getText());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
	}
	
	public void setRtmpTextField(final JTextField rtmpTextField) {
		rtmpTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						try {
							setRtmpPort(rtmpTextField.getText());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		});
	}

	private void setServer(String string) {
		relay.setServer(string);
	}

	private void setListenPort(String string) {
		relay.setListenPort(Integer.parseInt(string));
	}

	private void setRtmpPort(String string) {
		relay.setRtmpPort(Integer.parseInt(string));
		
	}

}
