package tw.cse.moa.stream;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class StreamClient extends Thread {

	private RTMPRelay relay;
	private Socket client;
	
	public StreamClient(RTMPRelay relay) {
		this.relay = relay;
	}

	private void connectToJTV() {
		client = new Socket();
		InetSocketAddress isa = new InetSocketAddress(relay.getServer(), relay.getRtmpPort());
		try {
			client.connect(isa, 10000);
			if(client.isConnected()){
				System.out.println(relay.getServer()+" is connected");
			}
			if (client.isClosed()) {
				System.out.println(client.getRemoteSocketAddress().toString() + " is disconnected!");
			}
			relay.setJtvOutput(client.getOutputStream());
			relay.setJtvInput(client.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		connectToJTV();
	}

	public void close() throws IOException {
		client.close();
		client = null;
	}
}
