package tw.cse.moa.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class LocalProxy extends Thread {

	private InputStream input;
	private OutputStream output;
	private byte[] buffer = new byte[65536];

	public LocalProxy(InputStream input, OutputStream output) {
		this.input = input;
		this.output = output;
		start();
	}

	public void run() {
		System.out.println("LocalProxy run");
		while (true) {
			try {
				proxy();
				Thread.sleep(1);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private int proxy() {
		int readSize = 0;

		try {
			if (input.available() > 0) {
				readSize = input.read(buffer);
			}
			if (readSize > 0) {
				output.write(buffer, 0, readSize);
			}
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		return readSize;
	}
}
